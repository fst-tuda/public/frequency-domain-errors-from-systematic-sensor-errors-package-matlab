%% Setup
clear

currentFileFullPath = mfilename('fullpath');
[dirpath,~,~] = fileparts(currentFileFullPath);
% Path to the testing environment
projectPath = dirpath; 

disp("Running tests file 'test_propagateSysUncToFreqDomain.m'")
%% Tests; Normal functional test with pregenerated data
testing_mat_data_files = dir([projectPath, '/data', '/*.mat']);

for ii = 1:length(testing_mat_data_files)
    data_file_path = [projectPath, '/data/', testing_mat_data_files(ii).name];
    testfunction_propagateSysUncToFreqDomain(data_file_path)
    
    disp(append("Test ", char(string(sprintf( '%01d', ii))), ...
                " with testing file '", testing_mat_data_files(ii).name, ...
                "' successfully finished!"))
end

%% Test data structure
% TODO:
%testDataStructure(data)

%% Test Plots
% figure(1)
% plot(result.FFT.frequencies,abs(result.FFT.mean(1:length(result.FFT.frequencies))),'o')

disp("Successfully finished running the tests file 'test_propagateSysUncToFreqDomain.m'")



%% Testing functions
function testfunction_propagateSysUncToFreqDomain(data_file_path)
    test_data = load(data_file_path);
    result = propagateSysUncToFreqDomain(...
                test_data.data.value, ...
                test_data.data.time.value, ...
                test_data.data.time.sampletime, ...
                'bias', test_data.data.uncertainty.systematic.bias, ...
                'slope', test_data.data.uncertainty.systematic.slope, ...
                'lin', test_data.data.uncertainty.systematic.linearity, ...
                'hys', test_data.data.uncertainty.systematic.hysteresis ...
             );

    % TODO: Better assert error messages
    assert(result.FFT.uncertainty.systematic.absolute ...
                == test_data.result.FFT.uncertainty.systematic.absolute, ...
           'result.FFT.uncertainty.systematic.absolute is not the awaited value');
    assert(isequal(result.FFT.uncertainty.systematic.phase, ...
                   test_data.result.FFT.uncertainty.systematic.phase), ...
           'result.FFT.uncertainty.systematic.phase is not the awaited value');
    
    clearvars test_data result
end





