function [data] = getSampleData(amp, freq, nCycles, sampleTime)
%getSampleData Summary of this function goes here
%   Detailed explanation goes here

data.time.sampletime = sampleTime;
data.time.unit = "Seconds";
data.numberpoints = round(nCycles / (sampleTime*freq));
data.time.value = [0 : sampleTime : data.numberpoints * sampleTime];
data.numberpoints = data.numberpoints + 1;
data.value = amp * sin(2 * pi * freq * data.time.value);
data.unit = "";

end

