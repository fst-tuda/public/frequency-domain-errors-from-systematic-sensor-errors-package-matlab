# Frequency Domain Errors From Systematic Sensor Errors package matlab
## Introduction
This is the repository mentioned in the "Propagation of Systematic Sensor Errors into the Frequency Domain - A MATLAB Software Framework" paper (see https://dx.doi.org/10.2139/ssrn.4452038 )
that provides a small matlab package with one main function with which you are able to calculate the error in the Frequency Domain (calculated with a FFT) by the given
arguments of one harmonic oscillation input signal with at least one complete cycle in the format of a one dimensional array y(t) and the systematic uncertainty,
which can be obtained from the data sheet or the calibration protocol of the sensor. <br>

Please Note: <br>
1. Measurementnoise can be handled by the framework
2. The systematic uncertainty is a collective term that can be calculated from other errors. For example the a) bias error b) sensitivity error, c) linearity
error and d) hysteresis error. <br>

The package uses a sophisticated sensor error model in combination with a easy mathematical correlation expression, obtained with Monte Carlo simulations, to propagate the
modelled errors into the frequency domain. The package intends to enable the user to easily determine the statistical errors
and to transfer them together with the given systematic errors into the frequency domain. Therefore, the software package
represents a significant contribution in the realm of uncertainty propagation, especially in the field of system identification. <br>


## Associated Publications
**The associated paper:** <br>
Rexer, Manuel and Pelz, Peter F. and Kuhr, Maximilian M.G., Uncertainty Propagation of Systematic Sensor Errors into the Frequency Domain. Available at SSRN: https://ssrn.com/abstract=4452038 or http://dx.doi.org/10.2139/ssrn.4452038  <br>

**The publication of this software package on Zenodo to obtain a global persistent Identifier (persitent ID):** <br>
TODO:

## Getting started
The only API function this package provides is the `calculateFrequencyDomainERRfromSystematicSensorERR`(...) function
```matlab
result = ...
    calculateFrequencyDomainERRfromSystematicSensorERR(...
                                            value_vector, ...
                                            time_vector, ...
                                            sampletime, ...
                                            'bias', 0.01, ...
                                            'slope', 0.02, ...
                                            'lin', 0.03, ...
                                            'hys', 0.04);
```
Where <br>
1. `value_vector` must be (1-BY-N) vector with your data,
2. `time_vector` must be (1-BY-N) vector with the corresponding time values,
3. `sampletime` a scalar (1-BY-1) real number of type double that is the step time between the single values of the time_vector and
4. `'bias'` `'slope'` `'lin'` `'hys'` **optional arguments** for the different systematic uncertainties "bias", "slope", "linearity" and "hysteresis" which can be usually found in the data sheet or the calibration protocol of the sensor you have used to record the data signal.
The "linearity" and "hysteresis" systematic uncertainties are not always given by the datasheets and, if left out in the function call, assumed as 0. <br>

Returns: <br>
`result_struct` that contains the input data, interim results, the absolute systematic uncertainty in the frequency domain and the phase systematic uncertainty for every frequency gathered from the FFT. <br>

**TODO: explaination of the data structure** 

## Contributing
If you find errors in this software please open a issue on the corresponding RWTH-GitLab repository.
If you encounter severe problems or nobody answers on your issue (after 2-3 weeks) you can also try to contact the current maintainers through their emails, or better contact the secretariat of the Chair of Fluid Systems (Institut für Fluidsystemtechnik) through
the contact person mentioned on https://www.fst.tu-darmstadt.de/fachgebiet/mitarbeiter/index.en.jsp . <br>
Thank you! <br>

## Current maintainers (Latest state October 2023):
Manuel Rexer, Main Author of the paper. manuel.rexer[at]tu-darmstadt.de <br>
Sebastian Neumeier, Research Aide (german: SHK) with the responsibility to refactor the code and maintain the repository. sebastian.neumeier[at]stud.tu-darmstadt.de

## Improvement Suggestions:
1. Explaination of the returned data structure.
2. If there should be advances in the use of RDF in calculation packages/software we should come back to the returned data structure and implement it there.
3. Maybe add a real world example with a plot of a data signal as a small 'how-to guide' on how this package could be used correctly.


