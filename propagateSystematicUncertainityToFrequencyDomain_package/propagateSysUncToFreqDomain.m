function result_struct = propagateSysUncToFreqDomain(value_vector, time_vector, sampletime, varargin)
% PROPAGATESYSUNCTOFREQDOMAIN 
%
%   result_struct = propagateSysUncToFreqDomain( ...
%                                               value_vector,
%                                               time_vector,
%                                               sampletime,
%                                               varargin)
%
%   -- value_vector  needs to be a 1-by-N vector containing the values of
%   the signal.
%
%   -- time_vector  needs to be a 1-by-N vector containing the time values
%   of the signal.
%
%   -- sampletime  needs to be a scalar natural number (1-by-1) of type
%   integer that describes the time between steps of the time_vector.
%
%   -- varargin (optional parameters)
%       -- 'bias'  scalar (1-by-1) real number of type double that
%       represents the *bias* systematic uncertainty and can be obtained
%       from the data sheet of the sensor that was used for the
%       measurement or from calibration protocols.
%
%       -- 'slope'  scalar (1-by-1) real number of type double that
%       represents the *slope* systematic uncertainty and can be obtained
%       from the data sheet of the sensor that was used for the
%       measurement or from calibration protocols.
%
%       -- 'lin'  scalar (1-by-1) real number of type double that
%       represents the *linearity* systematic uncertainty and can be
%       *sometimes (not always given)* obtained from the data sheet of the
%       sensor that was used for the measurement or from calibration
%       protocols.
%       Use 0 if there isn't a given value for your sensor.
%
%       -- 'hys'  scalar (1-by-1) real number of type double that
%       represents the *hysteresis* systematic uncertainty and can be
%       *sometimes (not always given)* obtained from the data sheet of the
%       sensor that was used for the measurement  or from calibration
%       protocols.
%       Use 0 if there isn't a given value for your sensor.
%
%
%   Returns:
%   -- result_struct  A struct that contains the input data, interim
%   results, the absolute systematic uncertainty in the frequency
%   domain and the phase systematic uncertainty for every frequency
%   gathered from the FFT. 
%
%   -- TODO: explanation of the data structure
%
%

%% Input adaption
defaultBias = 0;
defaultSlope = 0;
defaultLin = 0;
defaultHys = 0;

p = inputParser;
validScalarPosNum = @(x) isnumeric(x) && (x > 0);

addOptional(p, 'bias', defaultBias, validScalarPosNum);
addOptional(p, 'slope', defaultSlope, validScalarPosNum);
addOptional(p, 'lin', defaultLin, validScalarPosNum);
addOptional(p, 'hys', defaultHys, validScalarPosNum);

parse(p, varargin{:})

%% Generate the uncertainty data structure with the given input arguments
data.uncertainty.systematic.bias = p.Results.bias;
data.uncertainty.systematic.slope = p.Results.slope;
data.uncertainty.systematic.lin = p.Results.lin;
data.uncertainty.systematic.hys = p.Results.hys;


%% Pre Process the data
% Analyse main frequency
result_struct.excitation_frequency = getMainFreq(value_vector, time_vector);

% Get number of cycles
result_struct.number_of_cycles = getNumberOfCycles(time_vector, result_struct.excitation_frequency);

% Crop the number of cycles
[result_struct.valuecrop, ...
 result_struct.timecrop, ...
 result_struct.number_of_cycles] = cropData( ...
                                        value_vector, ...
                                        time_vector, ...
                                        sampletime, ...
                                        result_struct.excitation_frequency, ...
                                        result_struct.number_of_cycles);

% Statistical analysis
[result_struct.value_mean_vector, ...
 result_struct.value_standartdeviation_vector] ...
                    = getTheStatisticOscillation( ...
                                        result_struct.valuecrop, ...
                                        result_struct.number_of_cycles);

%% FFT with uncertainty 
% FFT of mean values
result_struct.FFT.N_data_points_in_FFT = length(result_struct.value_mean_vector);
result_struct.FFT.mean = fft(result_struct.value_mean_vector);

% Calculate the statistical uncertainty
% TODO: Maybe also write a test for the statistical uncertainty
temp_term1 = result_struct.value_standartdeviation_vector * tinv(1 - (0.05/2), result_struct.number_of_cycles-1);
temp_term2 = (sqrt(result_struct.number_of_cycles))^2 * result_struct.FFT.N_data_points_in_FFT;
result_struct.FFT.uncertainty.statistical = sqrt(sum(temp_term1 / temp_term2));
clearvars temp_term1 temp_term2

% Scale coefficients of the FFT
result_struct.FFT.mean = 2 / result_struct.FFT.N_data_points_in_FFT * result_struct.FFT.mean ;
result_struct.FFT.mean(1) = result_struct.FFT.mean(1) / 2;

% Get frequency vector of the FFT
temp_term1 = (0:(result_struct.FFT.N_data_points_in_FFT / 2));
temp_term2 = (result_struct.FFT.N_data_points_in_FFT * sampletime);
result_struct.FFT.frequencies = temp_term1 / temp_term2; 
clearvars temp_term1 temp_term2

% Propagate the systematic uncertainty into the frequency domain
[result_struct.FFT.uncertainty.systematic.absolute, ...
 result_struct.FFT.uncertainty.systematic.phase] = ...
                calculateSysUncInFreqDomain(...
                                    data.uncertainty.systematic.bias, ...
                                    data.uncertainty.systematic.slope, ...
                                    data.uncertainty.systematic.lin, ...
                                    data.uncertainty.systematic.hys, ...
                                    result_struct.FFT.mean);

end

