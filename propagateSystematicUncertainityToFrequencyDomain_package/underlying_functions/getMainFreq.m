function main_frequency = getMainFreq(value_vector, time_vector)
% GETMAINFREQ gets the main frequency of a harmonic oscillations signal
%
%   main_frequency = GETMAINFREQ(value_vector, time_vector)
%   gets the main frequency of a harmonic oscillations signal described by
%   a value vector and time vector through regression analysis. 
%   It uses Mathworks Matlab fit(...) from the 'Mathworks Matlab Curve
%   Fitting Toolbox' with the 'fourier1' option. 
%
%   -- value_vector  needs to be a 1-by-N vector containing the values of
%   the signal.
%
%   -- time_vector  needs to be a 1-by-N vector containing the time values
%   of the signal.
%
%
%   Returns:
%   -- main_frequency  as a scalar number of type double.
%
%
%   See also FIT.


y = value_vector;
t = time_vector;
[c2,~] = fit(t', y', 'fourier1');
main_frequency = c2.w / (2*pi);

end

