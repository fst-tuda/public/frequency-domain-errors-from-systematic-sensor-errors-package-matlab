function [cropped_value_vector, ...
          cropped_time_vector, ...
          floored_number_of_cycles] = cropData(...
                                             data_vector,...
                                             time_vector, ...
                                             sampletime, ...
                                             main_frequency, ...
                                             number_of_cycles)
% CROPDATA Crops a harmonic osciallation signal to N (numberofcycles) full
% oscillations beginning at the start of the signal
%
%   [cropped_value_vector, cropped_time_vector, floored_numberofcycles] =
%       CROPDATA(data_vector,
%                time_vector,
%                sampletime,
%                main_frequency,
%                numberofcycles) 
%
%   -- data_vector  needs to be a 1-by-N vector containing the values of
%   the signal.
%
%   -- time_vector  needs to be a 1-by-N vector containing the time values
%   of the signal.
%
%   -- sampletime  needs to be a scalar natural number (1-by-1) of type
%   integer that describes the time between steps of the time_vector.
%
%   -- main_frequency  needs to be a scalar number (1-by-1) of type double
%   or int and can be obtained through the getMainFreq(...) function of 
%   this package this file also belongs to.
%
%   -- numberofcycles  as a scalar real number of type double and can be
%   obtained through the getNumberOfCycles(...) function of this package
%   this file also belongs to.
%
%
%   Returns:
%   -- cropped_value_vector  the 1-by-N value vector cropped down to a
%   length that only contains a natural number of full harmonic
%   oscillations/cycles containing the values of the signal.
%
%   -- cropped_time_vector  the 1-by-N time vector cropped down to a length
%   that only contains a natural number of full harmonic
%   oscillations/cycles containing the values of the signal.
%
%   -- floored_number_of_cycles  as a scalar natural number of type integer.
%   It is the rounded down (to a natural number) version of the input
%   numberofcycles.
%
%
%   See also GETMAINFREQ, GETNUMBEROFCYCLES.


y = data_vector;
t = time_vector;
floored_number_of_cycles = floor(number_of_cycles);
Ndft = round((floored_number_of_cycles * (1/main_frequency)) / sampletime);

% Cut n-periods  from the timeseries
cropped_value_vector = y(1:Ndft);
cropped_time_vector = t(1:Ndft);
    
end

