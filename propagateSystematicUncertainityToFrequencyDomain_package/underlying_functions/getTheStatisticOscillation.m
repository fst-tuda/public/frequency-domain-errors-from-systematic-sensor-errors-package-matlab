function [value_mean_vector, value_standartdeviation_vector] = getTheStatisticOscillation(cropped_value_vector, floored_number_of_cycles)
% GETSTATISTICOSCILLATION get the "statistic oscillation" (mean value
% vector and the standart deviation vector) by taking all available cycles
% of the harmonic osciallation, "moving them over each other" and
% calculating the mean and standart deviation for every data points that are
% superimposed upon each other.
%   [value_mean_vector,
%    value_standartdeviation_vector] = 
%               getTheStatisticOscillation( ...
%                                          cropped_value_vector
%                                          floored_number_of_cycles)
%
%   -- cropped_value_vector  needs to be a 1-by-N vector containing the
%   values of the signal. It needs to be cropped to a length that
%   only a natural number of *full* harmonic oscillations/cycles are
%   present. The maximum amount of *full* harmonic oscillations/cycles is
%   the optimum you should use.
%
%   -- floored_numberofcycles  as a scalar natural number of type integer.
%   It is the rounded down (to a natural number) version of the input
%   number_of_cycles.
%
%
%   Returns:
%   -- value_mean_vector  the 1-by-N value vector of the "statistic
%   oscillation" that has the length of
%   length(cropped_value_vector)/floored_numberofcycles.
%
%   -- value_standartdeviation_vector  the 1-by-N value standart deviation
%   vector of the "statistic oscillation" that also has the length of
%   length(cropped_value_vector)/floored_numberofcycles.
%
%
%   See also GETNUMBEROFCYCLES.


data_points_per_cycle = round(length(cropped_value_vector) / floored_number_of_cycles) - 1;
data_matrix = [];

start_index = 1;
for ii = 1:floored_number_of_cycles
    end_index = start_index + data_points_per_cycle;
    data_matrix(ii,:) = cropped_value_vector(start_index : end_index);
    % For every oscillation reset the start_index that only one
    % oscillation/cycle gets handled.
    start_index = end_index + 1;
end

for jj = 1:(data_points_per_cycle + 1)
    value_mean_vector(jj) = mean(data_matrix(:, jj));
    value_standartdeviation_vector(jj) = std(data_matrix(:, jj));
end

end

