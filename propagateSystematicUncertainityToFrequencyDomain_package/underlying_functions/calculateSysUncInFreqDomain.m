function [systematic_uncertainty_absolute, ...
          systematic_uncertainty_phase] = ...
                    calculateSysUncInFreqDomain( ...
                                                sysunc_bias, ...
                                                sysunc_slope, ...
                                                sysunc_linearity, ...
                                                sysunc_hysteresis, ...
                                                FFT_Mean)
% CALCULATE SYSUNCINFREQDOMAIN Calculate the systematic uncertainty in the
% frequency domain with the mathematical expression given by the paper
% mentioned in the README.md of this repository/package.
%
%   [systematic_uncertainty_absolute, ...
%     systematic_uncertainty_phase] = ...
%               calculateSysUncInFreqDomain( ...
%                                           sysunc_bias, ...
%                                           sysunc_slope, ...
%                                           sysunc_linearity, ...
%                                           sysunc_hysteresis, ...
%                                           FFT_Mean)
%
%   -- sysunc_bias  scalar (1-by-1) real number of type double that
%   represents the *bias* systematic uncertainty and can be obtained from
%   the data sheet of the sensor that was used for the measurement or from 
%   calibration protocols.
%
%   -- sysunc_slope  scalar (1-by-1) real number of type double that
%   represents the *slope* systematic uncertainty and can be obtained from
%   the data sheet of the sensor that was used for the measurement or from 
%   calibration protocols.
%
%   -- sysunc_linearity  scalar (1-by-1) real number of type double that
%   represents the *linearity* systematic uncertainty and can be *sometimes
%   (not always given)* obtained from the data sheet of the sensor that was
%   used for the measurement or from calibration protocols. 
%   Use 0 if there isn't a given value for your sensor.  

%   -- sysunc_hysteresis  scalar (1-by-1) real number of type double that
%   represents the *hysteresis* systematic uncertainty and can be
%   *sometimes (not always given)* obtained from the data sheet of the
%   sensor that was used for the measurement or from calibration protocols.
%   Use 0 if there isn't a given value for your sensor.

%   -- FFT_Mean  FFT of the mean value vector of the "statistic
%   oscillation".
%
%
%   Returns:
%   -- systematic_uncertainty_absolute  scalar (1-by-1) real number of type
%   'double' that represents the absolute systematic uncertainty of the
%   measurement signal with the given sensor in the frequency domain.
%
%   -- systematic_uncertainty_phase (1-by-N) vector with real numbers of
%   type 'double' that represents the systematic uncertainty of the phase in
%   the different frequencies of the FFT of the measurement signal with the
%   given sensor. 


systematic_uncertainty_absolute = sysunc_bias + sysunc_slope + (0.608 * sysunc_linearity);
systematic_uncertainty_phase = atan(0.681 * sysunc_hysteresis ./ abs(FFT_Mean));

end

