function number_of_cycles = getNumberOfCycles(time_vector, main_frequency)
% GETNUMBEROFCYCLES returns the count of full oscillations of a signal
%
%   number_of_cycles = GETNUMBEROFCYCLES(time_vector, main_frequency)
%       returns the count of full oscillations of the signal given by the 
%       time_vector and the main frequency of the value vector.
%
%   -- time_vector  needs to be a 1-by-N vector containing the time values
%   of the signal.
%
%   -- main_frequency  needs to be a scalar number (1-by-1) of type 'double'
%   or 'int' and can be obtained through the getMainFreq(...) function of
%   this package this file also belongs to. 
%
%   Returns:
%   -- number_of_cycles  as a scalar real number of type 'double'.
%
%
%   See also GETMAINFREQ.


t = time_vector;
number_of_cycles = (t(end) - t(1)) / (1/main_frequency);

if number_of_cycles < 1
    error(append("There is not at least one complete harmonic cycle in", ...
                 "the signal you have provided. The data cannot be", ...
                 "analyzed."))
end

end

